package ru.neomer;

public interface ContactRecord {

    String getValue();

    void changeValue(String value);

    String getType();

}
