package ru.neomer;

public class CheckAmountDecorator implements BankAccount {

    private final BankAccount inner;

    public CheckAmountDecorator(BankAccount inner) {
        this.inner = inner;
    }

    @Override
    public double getAmount() {
        return this.inner.getAmount();
    }

    @Override
    public void take(double amount) throws Exception {
        if (getAmount() < amount) {
            throw new Exception("Too few cash.");
        }
        this.inner.take(amount);
    }
}
