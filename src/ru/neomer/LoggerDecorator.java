package ru.neomer;

public class LoggerDecorator implements BankAccount {

    private final BankAccount inner;

    public LoggerDecorator(BankAccount inner) {
        this.inner = inner;
    }

    @Override
    public double getAmount() {
        return this.inner.getAmount();
    }

    @Override
    public void take(double amount) throws Exception {
        System.out.println("Current cash: " + getAmount());
        System.out.println("Try to take amount: " + amount);
        this.inner.take(amount);
    }
}
