package ru.neomer;

public class PrettyPrintDecorator implements ContactRecord {

    private ContactRecord inner;

    public PrettyPrintDecorator(PhoneContactRecord inner) {
        this.inner = inner;
    }

    @Override
    public String getValue() {
        String value = this.inner.getValue();
        return "8 (" + value.substring(1, 4) + ") " +
                value.substring(4, 7) + "-" +
                value.substring(7, 9) + "-" +
                value.substring(9, 11);
    }

    @Override
    public void changeValue(String value) {
        this.inner.changeValue(value);
    }

    @Override
    public String getType() {
        return this.inner.getType();
    }
}
