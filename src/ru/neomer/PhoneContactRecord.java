package ru.neomer;

public class PhoneContactRecord implements ContactRecord {

    private String phone;

    public PhoneContactRecord(String phone) {
        this.phone = phone;
    }

    @Override
    public String getValue() {
        return this.phone;
    }

    @Override
    public void changeValue(String value) {
        this.phone = value;
    }

    @Override
    public String getType() {
        return "MobilePhone";
    }
}
