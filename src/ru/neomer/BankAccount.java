package ru.neomer;

public interface BankAccount {

    double getAmount();

    void take(double amount) throws Exception;
}
