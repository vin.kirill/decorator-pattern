package ru.neomer;

public class Main {

    public static void main(String[] args) throws Exception {

        ContactRecord phone = new PrettyPrintDecorator(new PhoneContactRecord("89120106724"));

        printRecord(phone);
    }

    private static void printRecord(ContactRecord record) {
        System.out.println(record.getType() + ": " + record.getValue());
    }
}
