package ru.neomer;

public class BankAccountImpl implements BankAccount {

    private double amount;

    public BankAccountImpl(double amount) {
        this.amount = amount;
    }

    @Override
    public double getAmount() {
        return amount;
    }

    @Override
    public void take(double amount) throws Exception {
        this.amount -= amount;
    }
}
